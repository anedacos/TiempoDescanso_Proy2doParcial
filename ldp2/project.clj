(defproject ldp2 "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [incanter "1.9.3"]
                 [svm-clj "0.1.3"]]
  :main ^:skip-aot ldp2.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})

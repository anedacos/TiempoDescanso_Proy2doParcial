(ns ldp2.core
  (:gen-class)
  (:require [svm.core :as svm]
            [incanter.core :as ican]
            [incanter.stats :as icanst]
            [incanter.charts :as icanch]
            [incanter.io :as icanio])
    (:import (javax.swing SwingUtilities JFrame JLabel JTextField JButton JPanel JOptionPane)
           (java.awt BorderLayout)
           (java.awt FlowLayout)
           (java.awt.event ActionListener)))

(defn -main [& args]

  ;Lectura de archivo de clase 1
  (def dataClase1 (icanio/read-dataset "resources/data1.csv" :header true))
  
  ;Visualizar dataset de clase 1
  (ican/view dataClase1)
  
  ;;MODELO DE REGRESION LINEAL SIMPLE
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;remperatura-humedad
  (def temperatura-c1 (ican/sel dataClase1 :cols :temperatura))
  
  (def humedad-c1 (ican/sel dataClase1 :cols :humedad))
  
  (def linear-samp-scatter-c1
    (icanch/scatter-plot temperatura-c1 humedad-c1
                         :title "REGRESION LINEAL SIMPLE CLASE 1: CALOR CON RUIDO"
                         :x-label "Temperatura"
                         :y-label "Humedad"))
  
  (defn plot-scatter-c1 []
    (ican/view linear-samp-scatter-c1))
  
  (def samp-linear-model-c1
    (icanst/linear-model humedad-c1 temperatura-c1))
  
  ;Mostramos plot de Humedad vs. Temperatura
  (defn plot-model-c1 []
    (ican/view
      (icanch/add-lines linear-samp-scatter-c1
                        temperatura-c1 (:fitted samp-linear-model-c1
                                 ))))
  (plot-model-c1)
  
  ;;Convertir a to-matrix
  (def datamatrix (ican/to-matrix dataClase1))
  
  ;;PREDICCIÓN - REGRESION LINEAL SIMPLE
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;Coeficientes generados por el modelo
  (def coefs-c1 (:coefs samp-linear-model-c1))
  (def mse-c1 (:mse samp-linear-model-c1))
  
  (println "DATOS REGRESION LINEAL - C1")
  (println "*************************************")
  (println "Coeficientes : " coefs-c1)
  (println "Error de prediccion: "  mse-c1)
  
  
  
  ;Lectura de archivo de clase 2
  (def dataClase2 (icanio/read-dataset "resources/data2.csv" :header true))
  
  ;Visualizar dataset de clase 2
  (ican/view dataClase2)
  
  ;;MODELO DE REGRESION LINEAL SIMPLE
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;remperatura-humedad
  (def temperatura-c2 (ican/sel dataClase2 :cols :temperatura))
  
  (def humedad-c2 (ican/sel dataClase2 :cols :humedad))
  
  (def linear-samp-scatter-c2
    (icanch/scatter-plot temperatura-c2 humedad-c2
                         :title "REGRESION LINEAL SIMPLE CLASE 2: CALOR SIN RUIDO"
                         :x-label "Temperatura"
                         :y-label "Humedad"))
  
  (defn plot-scatter-c2 []
    (ican/view linear-samp-scatter-c2))
  
  (def samp-linear-model-c2
    (icanst/linear-model humedad-c2 temperatura-c2))
  
  ;Mostramos plot de Humedad vs. Temperatura
  (defn plot-model-c2 []
    (ican/view
      (icanch/add-lines linear-samp-scatter-c2
                        temperatura-c2 (:fitted samp-linear-model-c2
                                 ))))
  (plot-model-c2)
  
  ;;Convertir a to-matrix
  (def datamatrix (ican/to-matrix dataClase2))
  
  ;;PREDICCIÓN - REGRESION LINEAL SIMPLE
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;Coeficientes generados por el modelo
  (def coefs-c2 (:coefs samp-linear-model-c2))
  (def mse-c2 (:mse samp-linear-model-c2))
  
  (println "DATOS REGRESION LINEAL - C2")
  (println "*************************************")
  (println "Coeficientes : " coefs-c2)
  (println "Error de prediccion: "  mse-c2)
  
  
  ;Lectura de archivo de clase 3
  (def dataClase3 (icanio/read-dataset "resources/data3.csv" :header true))
  
  ;Visualizar dataset de clase 3
  (ican/view dataClase3)
  
  ;;MODELO DE REGRESION LINEAL SIMPLE
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;remperatura-humedad
  (def temperatura-c3 (ican/sel dataClase3 :cols :temperatura))
  
  (def humedad-c3 (ican/sel dataClase3 :cols :humedad))
  
  (def linear-samp-scatter-c3
    (icanch/scatter-plot temperatura-c3 humedad-c3
                         :title "REGRESION LINEAL SIMPLE CLASE 3: FRIO SIN RUIDO"
                         :x-label "Temperatura"
                         :y-label "Humedad"))
  
  (defn plot-scatter-c3 []
    (ican/view linear-samp-scatter-c3))
  
  (def samp-linear-model-c3
    (icanst/linear-model humedad-c3 temperatura-c3))
  
  ;Mostramos plot de Humedad vs. Temperatura
  (defn plot-model-c3 []
    (ican/view
      (icanch/add-lines linear-samp-scatter-c3
                        temperatura-c3 (:fitted samp-linear-model-c3
                                 ))))
  (plot-model-c3)
  
  ;;Convertir a to-matrix
  (def datamatrix (ican/to-matrix dataClase3))
  
  ;;PREDICCIÓN - REGRESION LINEAL SIMPLE
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;Coeficientes generados por el modelo
  (def coefs-c3 (:coefs samp-linear-model-c3))
  (def mse-c3 (:mse samp-linear-model-c3))
  
  (println "DATOS REGRESION LINEAL - C3")
  (println "*************************************")
  (println "Coeficientes : " coefs-c3)
  (println "Error de prediccion: "  mse-c3)
  
  
  ;Lectura de archivo de clase 4
  (def dataClase4 (icanio/read-dataset "resources/data4.csv" :header true))
  
  ;Visualizar dataset de clase 4
  (ican/view dataClase4)
  
  ;;MODELO DE REGRESION LINEAL SIMPLE
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;remperatura-humedad
  (def temperatura-c4 (ican/sel dataClase4 :cols :temperatura))
  
  (def humedad-c4 (ican/sel dataClase4 :cols :humedad))
  
  (def linear-samp-scatter-c4
    (icanch/scatter-plot temperatura-c4 humedad-c4
                         :title "REGRESION LINEAL SIMPLE CLASE 4: FRIO CON RUIDO"
                         :x-label "Temperatura"
                         :y-label "Humedad"))
  
  (defn plot-scatter-c4 []
    (ican/view linear-samp-scatter-c4))
  
  (def samp-linear-model-c4
    (icanst/linear-model humedad-c4 temperatura-c4))
  
  ;Mostramos plot de Humedad vs. Temperatura
  (defn plot-model-c4 []
    (ican/view
      (icanch/add-lines linear-samp-scatter-c4
                        temperatura-c4 (:fitted samp-linear-model-c4
                                 ))))
  (plot-model-c4)
  
  ;;Convertir a to-matrix
  (def datamatrix (ican/to-matrix dataClase4))
  
  ;;PREDICCIÓN - REGRESION LINEAL SIMPLE
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;Coeficientes generados por el modelo
  (def coefs-c4 (:coefs samp-linear-model-c4))
  (def mse-c4 (:mse samp-linear-model-c4))
  
  (println "DATOS REGRESION LINEAL - C4")
  (println "*************************************")
  (println "Coeficientes : " coefs-c4)
  (println "Error de prediccion: "  mse-c4)
  
  
  ;Lectura de archivo de clase 5
  (def dataClase5 (icanio/read-dataset "resources/data5.csv" :header true))
  
  ;Visualizar dataset de clase 5
  (ican/view dataClase5)
  
  ;;MODELO DE REGRESION LINEAL SIMPLE
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;remperatura-humedad
  (def temperatura-c5 (ican/sel dataClase5 :cols :temperatura))
  
  (def humedad-c5 (ican/sel dataClase5 :cols :humedad))
  
  (def linear-samp-scatter-c5
    (icanch/scatter-plot temperatura-c5 humedad-c5
                         :title "REGRESION LINEAL SIMPLE CLASE 5: RUIDO"
                         :x-label "Temperatura"
                         :y-label "Humedad"))
  
  (defn plot-scatter-c5 []
    (ican/view linear-samp-scatter-c5))
  
  (def samp-linear-model-c5
    (icanst/linear-model humedad-c5 temperatura-c5))
  
  ;Mostramos plot de Humedad vs. Temperatura
  (defn plot-model-c5 []
    (ican/view
      (icanch/add-lines linear-samp-scatter-c5
                        temperatura-c5 (:fitted samp-linear-model-c5
                                 ))))
  (plot-model-c5)
  
  ;;Convertir a to-matrix
  (def datamatrix (ican/to-matrix dataClase5))
  
  ;;PREDICCIÓN - REGRESION LINEAL SIMPLE
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;Coeficientes generados por el modelo
  (def coefs-c5 (:coefs samp-linear-model-c5))
  (def mse-c5 (:mse samp-linear-model-c5))
  
  (println "DATOS REGRESION LINEAL - C5")
  (println "*************************************")
  (println "Coeficientes : " coefs-c5)
  (println "Error de prediccion: "  mse-c5)
  
  
  ;Lectura de archivo de clase 6
  (def dataClase6 (icanio/read-dataset "resources/data6.csv" :header true))
  
  ;Visualizar dataset de clase 6
  (ican/view dataClase6)
  
  ;;MODELO DE REGRESION LINEAL SIMPLE
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;remperatura-humedad
  (def temperatura-c6 (ican/sel dataClase6 :cols :temperatura))
  
  (def humedad-c6 (ican/sel dataClase6 :cols :humedad))
  
  (def linear-samp-scatter-c6
    (icanch/scatter-plot temperatura-c6 humedad-c6
                         :title "REGRESION LINEAL SIMPLE CLASE 6: SIN RUIDO"
                         :x-label "Temperatura"
                         :y-label "Humedad"))
  
  (defn plot-scatter-c6 []
    (ican/view linear-samp-scatter-c6))
  
  (def samp-linear-model-c6
    (icanst/linear-model humedad-c6 temperatura-c6))
  
  ;Mostramos plot de Humedad vs. Temperatura
  (defn plot-model-c6 []
    (ican/view
      (icanch/add-lines linear-samp-scatter-c6
                        temperatura-c6 (:fitted samp-linear-model-c6
                                 ))))
  (plot-model-c6)
  
  ;;Convertir a to-matrix
  (def datamatrix (ican/to-matrix dataClase6))
  
  ;;PREDICCIÓN - REGRESION LINEAL SIMPLE
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;Coeficientes generados por el modelo
  (def coefs-c6 (:coefs samp-linear-model-c6))
  (def mse-c6 (:mse samp-linear-model-c6))
  
  (println "DATOS REGRESION LINEAL - C6")
  (println "*************************************")
  (println "Coeficientes : " coefs-c6)
  (println "Error de prediccion: "  mse-c6)
  
  
  ;Lectura de archivo
  (def data (icanio/read-dataset "resources/dataset.csv" :header true))
  
  ;Visualizar dataset
  (ican/view data)
  
  ;;MODELO DE REGRESION LINEAL SIMPLE
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;remperatura-humedad
  (def temperatura (ican/sel data :cols :temperatura))
  
  (def humedad (ican/sel data :cols :humedad))
  
  (def linear-samp-scatter
    (icanch/scatter-plot temperatura humedad
                         :title "REGRESION LINEAL SIMPLE DATASET"
                         :x-label "Temperatura"
                         :y-label "Humedad"))
  
  (defn plot-scatter []
    (ican/view linear-samp-scatter))
  
  (def samp-linear-model
    (icanst/linear-model humedad temperatura))
  
  ;Mostramos plot de Humedad vs. Temperatura
  (defn plot-model []
    (ican/view
      (icanch/add-lines linear-samp-scatter
                        temperatura (:fitted samp-linear-model
                                 ))))
  (plot-model)
  
  ;;Convertir a to-matrix
  (def datamatrix (ican/to-matrix data))
  
  ;;PREDICCIÓN - REGRESION LINEAL SIMPLE
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;Coeficientes generados por el modelo
  (def coefs (:coefs samp-linear-model))
  (def mse (:mse samp-linear-model))
  
  (println "DATOS REGRESION LINEAL - DATASET")
  (println "*************************************")
  (println "Coeficientes : " coefs)
  (println "Error de prediccion: "  mse)
  
  
  
  ;(def col-ruido (ican/sel data :cols [:temperatura :ruido]))
  
  ;(ican/view col-ruido)
  
  ;(def data-m (ican/to-matrix data))
  
  ;(ican/view data-m)
  
  ;(def transpuesta (ican/dim data-m))
  
  ;(ican/view transpuesta)
  
  
  (def dataset (svm/read-dataset "resources/data"))
  
  (def model (svm/train-model dataset))

  (def feature (last (first dataset)))
  
  ;(def p {1 14, 2 20, 3 36})
  ;(defn p [t h r]
   ; (type {1 t, 2 h,3 r}))
  
  ;(println p)
  ;(defn p [t h r]
   ; (str {1 t, 2 h, 3 r}))
        
  (defn sugerencia-12 [clase]
    (let [n (+ 1 (rand-int 3))]
      (case n
        1 (str (str "Clase: " clase) "\nEstimados trabajadores: Dentro de 5 minutos se tomara un descanso de 10 minutos en el auditorio para refrescarnos")
        2 (str (str "Clase: " clase) "\nEstimados trabajadores: Dentro de 3 minutos se tomara un descanso de 10 minutos en el salon con musica relajante")
        3 (str (str "Clase: " clase) "\nEstimados trabajadores: Dentro de 5 minutos se tomara un descanso de 10 minutos en el comedor para un refrigerio")))) 


  (defn sugerencia-34 [clase]
    (let [n (+ 1 (rand-int 3))]
      (case n
        1 (str (str "Clase: " clase) "\nEstimados trabajadores: Dentro de 5 minutos se tomara un descanso de 10 minutos en el patio para recibir un poco de sol")
        2 (str (str "Clase: " clase) "\nEstimados trabajadores: Dentro de 3 minutos se tomara un descanso de 10 minutos en el salon con musica relajante")
        3 (str (str "Clase: " clase) "\nEstimados trabajadores: Dentro de 5 minutos se tomara un descanso de 10 minutos en la cafeteria para tomar un cafe y entrar en calor")))) 


  (defn sugerencia-56 [clase]
    (let [n (+ 1 (rand-int 3))]
      (case n
        1 (str (str "Clase: " clase) "\nEstimados trabajadores: Dentro de 5 minutos se tomara un descanso de 10 minutos en el auditorio para calmarnos")
        2 (str (str "Clase: " clase) "\nEstimados trabajadores: Dentro de 3 minutos se tomara un descanso de 10 minutos en el salon con musica relajante")
        3 (str (str "Clase: " clase) "\nEstimados trabajadores: Dentro de 5 minutos se tomara un descanso de 10 minutos en el patio para escuchar el cantar de las aves")))) 
  
  (defn predice [t h r] 
    (let [n (svm/predict model {1 (Integer/parseInt (.getText t)), 2 (Integer/parseInt (.getText h)), 3 (Integer/parseInt (.getText r))})]
      (cond
        (= n 1.0) (sugerencia-12 n)
        (= n 2.0) (sugerencia-12 n)
        (= n 3.0) (sugerencia-34 n)
        (= n 4.0) (sugerencia-34 n)
        (= n 5.0) (sugerencia-56 n)
        (= n 6.0) (sugerencia-56 n)))) 
  
    ;Interfaz para la aplicacion
  (let [f (JFrame. "ASIGNACION ESTRATEGICA DE TIEMPO")
        labeltitle (JLabel. "ASIGNACION ESTRATEGICA DE TIEMPO")
        labeltemperatura (JLabel. "TEMPERATURA:")
        labelhumedad (JLabel. "HUMEDAD:")
        labelruido (JLabel. "RUIDO:")
        txttemperatura (JTextField. "" 20)
        txthumedad (JTextField. "" 20)
        txtruido (JTextField. "" 20)        
        buttonpredecir (JButton. "SUGERENCIA" )
        panel (JPanel.) panel1 (JPanel.)
        f (proxy [JFrame ActionListener]
        [] ; superclass constructor arguments
        (actionPerformed [e] ; nil below is the parent component
        (JOptionPane/showMessageDialog
           nil (predice txttemperatura txthumedad txtruido))))]
    
    (doto panel
      (.add labeltitle)
      (.add labeltemperatura) (.add txttemperatura)
      (.add labelhumedad) (.add txthumedad)
      (.add labelruido) (.add txtruido)
      (.add buttonpredecir))
    
    (doto f
      (.setContentPane panel)    
      (.setDefaultCloseOperation JFrame/EXIT_ON_CLOSE)
      (.pack)
      (.setSize 270 225)
      (.setResizable false)
      (.setVisible true))
    (.addActionListener buttonpredecir f)
  )
  
  ;(def prediction (svm/predict model feature))
  
  
)
	
